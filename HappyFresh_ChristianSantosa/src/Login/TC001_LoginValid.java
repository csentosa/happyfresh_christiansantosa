package Login;

import static org.testng.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TC001_LoginValid extends base {
	@Test
	public void LoginValid() throws MalformedURLException, InterruptedException {
		// call initial to open APK from base
		AndroidDriver<AndroidElement> driver = capabilities();
		
		try {
			Thread.sleep(5000);
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_privacy_policy_button_agree")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_skip_on_boarding_button")).click();
			driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")).click();
			
			//direct to page account until page login 
			driver.findElement(By.id("com.happyfresh.staging:id/account_bottom_navigation")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_user_info_email")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/login")).click();
			
			//fill email & password
			driver.findElement(By.id("com.happyfresh.staging:id/email")).sendKeys("testchris@gmail.com");
			driver.findElement(By.id("com.happyfresh.staging:id/password")).sendKeys("qwerty123");
			
			//submit login
			driver.findElement(By.id("com.happyfresh.staging:id/login")).click();
			
			//check if user in page "My Account"
			String textPage = driver.findElement(By.xpath("//android.view.ViewGroup[@resource-id='com.happyfresh.staging:id/toolbar']/android.widget.TextView")).getText();
			Assert.assertEquals(textPage, "My Account");
			driver.quit();
			
		}catch (Exception e) {
			System.out.println("Something went wrong : " + e);
		}
		
	}
}
