package Login;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TC004_LoginEmptyFillPassword extends base {
	@Test
	public void LoginEmptyFillPassword() throws MalformedURLException, InterruptedException {
		// call initial to open APK from base
		AndroidDriver<AndroidElement> driver = capabilities();
		
		try {
			//this below script if there has on boarding screen, if there no have a boarding screen just comment this script below
			Thread.sleep(5000);
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_privacy_policy_button_agree")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_skip_on_boarding_button")).click();
			driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")).click();
			
			//direct to page account until page login 
			driver.findElement(By.id("com.happyfresh.staging:id/account_bottom_navigation")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_user_info_email")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/login")).click();
			
			//fill invalid email & valid password
			driver.findElement(By.id("com.happyfresh.staging:id/email")).sendKeys("testchris@gmail.com");
			
			//check button login is must disabled
			MobileElement element = (MobileElement) driver.findElement(By.id("com.happyfresh.staging:id/login"));
			boolean isEnabled = element.isEnabled();
			
			Assert.assertEquals(isEnabled, false);
			driver.quit();
			
		}catch (Exception e) {
			System.out.println("Something went wrong : " + e);
		}
	}
}
