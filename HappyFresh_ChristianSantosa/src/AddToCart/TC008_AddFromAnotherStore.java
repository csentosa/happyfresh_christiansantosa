package AddToCart;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TC008_AddFromAnotherStore extends base {
	@Test
	public void AddFromAnotherStore() throws MalformedURLException {
		// call initial to open APK from base
		AndroidDriver<AndroidElement> driver = capabilities();
		
		try {
			Thread.sleep(5000);
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_privacy_policy_button_agree")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_skip_on_boarding_button")).click();
			driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")).click();
			
			//direct to page account until page login 
			driver.findElement(By.id("com.happyfresh.staging:id/account_bottom_navigation")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_user_info_email")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/login")).click();
			
			//fill email & password
			driver.findElement(By.id("com.happyfresh.staging:id/email")).sendKeys("testchris@gmail.com");
			driver.findElement(By.id("com.happyfresh.staging:id/password")).sendKeys("qwerty123");
			
			//submit login
			driver.findElement(By.id("com.happyfresh.staging:id/login")).click();
			
			//back to home page
			Thread.sleep(5000);
			driver.findElement(By.xpath("//android.view.ViewGroup[@resource-id='com.happyfresh.staging:id/toolbar']/android.widget.ImageButton")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/home_bottom_navigation")).click();
			
			//set location
			driver.findElement(By.id("com.happyfresh.staging:id/component_address_info")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/search_src_text")).sendKeys("Cilandak town square");
			driver.findElement(By.xpath("//*[@text='Cilandak Town Square']")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_popup_dialog_primary_action_button")).click();
			
			//scroll to one stores (example : Super Indo)
			Thread.sleep(5000);
			driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Fulfilled by HappyFresh\"));");
			driver.findElement(By.xpath("//*[@text='Super Indo']")).click();
			
			//select one of product (example : Indomie Instant Fried Noodles)
			Thread.sleep(5000);
			driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Always cheaper\"));");
			int count = driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_product_name_text_view")).size();
			for(int i=0; i < count; i++) {
				String text = driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_product_name_text_view")).get(i).getText();
				if(text.equalsIgnoreCase("Indomie Instant Fried Noodles")) {
					driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_add_to_cart_button")).get(i).click();
					
					//check total count product that has been add to cart (in count per product)
					String countProduct = driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_product_cart_counter")).get(i).getText();
					Assert.assertEquals(countProduct, "1");
					break; // break when meet conditions
				}
			}
			
			//check total count all product at icon shop cart
			String countAllProductStoreSuperIndo = driver.findElement(By.id("com.happyfresh.staging:id/ui_view_cart_menu_badge")).getText();
			Assert.assertEquals(countAllProductStoreSuperIndo, "1");
			
			//restore all selected product for another test scenario
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_product_list_minus_cart_image_view")).click();
			
			//back to home page & go to store Carrefour HFC
			Thread.sleep(5000);
			driver.findElement(By.xpath("//android.view.ViewGroup[@resource-id='com.happyfresh.staging:id/component_toolbar_store_home_toolbar']/android.widget.ImageButton")).click();
			driver.findElement(By.xpath("//*[@text='Carrefour HFC']")).click();
			
			//select one of product (example : Indomie Instant Fried Noodles)
			Thread.sleep(5000);
			driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Always cheaper\"));");
			int countStoreCarrefour = driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_product_name_text_view")).size();
			for(int i=0; i < countStoreCarrefour; i++) {
				String text = driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_product_name_text_view")).get(i).getText();
				if(text.equalsIgnoreCase("Indomie Instant Fried Noodles")) {
					driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_add_to_cart_button")).get(i).click();
					
					//check total count product that has been add to cart (in count per product)
					String countProduct = driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_product_cart_counter")).get(i).getText();
					Assert.assertEquals(countProduct, "1");
					break; // break when meet conditions
				}
			}
			
			//check total count all product at icon shop cart
			String countAllProductStoreCarrefour = driver.findElement(By.id("com.happyfresh.staging:id/ui_view_cart_menu_badge")).getText();
			Assert.assertEquals(countAllProductStoreCarrefour, "1");
			
			//restore all selected product for another test scenario
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_product_list_minus_cart_image_view")).click();
			
			driver.quit();
			
		}catch (Exception e) {
			System.out.println("Something went wrong : " + e);
		}
	}
}
