package ProductListDetails;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TC011_DetailsProductOutOfStock extends base {
	@Test
	public void AddOutOfStock() throws MalformedURLException {
		// call initial to open APK from base
		AndroidDriver<AndroidElement> driver = capabilities();
		
		//define expected variable
		String selectedProduct = "Sunpride Cavendish Banana";
		String expectedNameProduct = "";
		
		try {
			Thread.sleep(5000);
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_privacy_policy_button_agree")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_skip_on_boarding_button")).click();
			driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")).click();
			
			//direct to page account until page login 
			driver.findElement(By.id("com.happyfresh.staging:id/account_bottom_navigation")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_user_info_email")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/login")).click();
			
			//fill email & password
			driver.findElement(By.id("com.happyfresh.staging:id/email")).sendKeys("testchris@gmail.com");
			driver.findElement(By.id("com.happyfresh.staging:id/password")).sendKeys("qwerty123");
			
			//submit login
			driver.findElement(By.id("com.happyfresh.staging:id/login")).click();
			
			//back to home page
			Thread.sleep(5000);
			driver.findElement(By.xpath("//android.view.ViewGroup[@resource-id='com.happyfresh.staging:id/toolbar']/android.widget.ImageButton")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/home_bottom_navigation")).click();
			
			//click next in navigation pop that shown up in home page
			driver.findElement(By.xpath("//*[@text='Next']")).click();
			
			//click Got it in navigation pop that shown up in home page
			driver.findElement(By.xpath("//*[@text='Got it']")).click();
			
			//set location
			driver.findElement(By.id("com.happyfresh.staging:id/component_address_info")).click();
			driver.findElement(By.id("com.happyfresh.staging:id/search_src_text")).sendKeys("Cilandak town square");
			driver.findElement(By.xpath("//*[@text='Cilandak Town Square']")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("com.happyfresh.staging:id/ui_view_popup_dialog_primary_action_button")).click();
			
			//scroll to one stores (example : Indoguna)
			Thread.sleep(5000);
			driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Supermarkets near you\"));");
			driver.findElement(By.xpath("//*[@text='All Freshh']")).click();
			
			//select one of product (example : Sunpride Cavendish Banana)
			Thread.sleep(5000);
			driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"A healthy start!\"));");
			int countOutOfStock = driver.findElements(By.xpath("//*[@text='OUT OF STOCK']")).size();
			for(int i=0; i < countOutOfStock; i++) {
				String text = driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_product_name_text_view")).get(i).getText();
				if(text.equalsIgnoreCase(selectedProduct)) {
					expectedNameProduct = driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_product_name_text_view")).get(i).getText();
					//check button login is must disabled
					MobileElement element = (MobileElement) driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_add_to_cart_button")).get(i);
					boolean isEnabled = element.isEnabled();
					
					//check button add must disabled
					Assert.assertEquals(isEnabled, true);
					
					driver.findElements(By.id("com.happyfresh.staging:id/ui_view_product_list_product_name_text_view")).get(i).click();
					break; // break when meet conditions
				}
			}
			
			//validation detail of products
			String resultNameProduct = driver.findElement(By.id("com.happyfresh.staging:id/ui_view_product_name_textview")).getText();
			String resultPriceProduct = driver.findElement(By.id("com.happyfresh.staging:id/ui_view_product_price_textview")).getText();
			MobileElement descProduct = (MobileElement) driver.findElement(By.id("com.happyfresh.staging:id/ui_view_product_description_textview"));
			boolean isDisplayed = descProduct.isDisplayed();
			
			Assert.assertEquals(resultNameProduct,expectedNameProduct);
			Assert.assertEquals(resultPriceProduct,"");
			Assert.assertEquals(isDisplayed, true);
			
			driver.quit();
			
		}catch (Exception e) {
			System.out.println("Something went wrong : " + e);
		}
	}
}
