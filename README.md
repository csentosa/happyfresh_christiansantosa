# HappyFresh_ChristianSantosa


## Getting started

1. Download Java.
2. Download Android Studio.
3. Download Node.
4. Set(Java, Android SDK & Node) Home Paths in Windows System variables.
5. Create and configure emulator in Android Studio with name "HappyFresh".
6. Instal appium server locally with node : npm install -g appium.


## How to Run Test

1. Scenario Test at folder Scenarios.
![scenarios.PNG](./scenarios.PNG)

2. Open emulator "HappyFresh" on Android Studio.

3. Start Appium server. Go to terminal and type "appium".
![startAppium.png](./startAppium.png)

4. Run test per function like this screenshot, example :
Right click at "testSuiteLogin.xml" > Run As > TestNG Suite.
![runtest.png](./runtest.png)
